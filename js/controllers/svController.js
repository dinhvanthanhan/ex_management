function layThongTinTuForm() {
    var maSv = document.getElementById("txtMaSV").value;
    var tenSv = document.getElementById("txtTenSV").value;
    var email = document.getElementById("txtEmail").value;
    var matKhau = document.getElementById("txtPass").value;
    var diemLy = +document.getElementById("txtDiemLy").value;
    var diemHoa = +document.getElementById("txtDiemHoa").value;
    var diemToan = +document.getElementById("txtDiemToan").value;

    // tạo object từ thông tin láy từ form
    var sv = new SinhVien(maSv, tenSv, email, matKhau, diemLy, diemToan, diemHoa);
    return sv;
}

function renderDssv(list) {
    // tbodySinhVien

    // render danh sách
    // contentHTML là 1 chuỗi chứa các thẻ tr sau này sẽ innerHTML vào thẻ tbody
    var contentHTML = "";

    for (var i = 0; i < list.length; i++) {
        var currentSv = dssv[i];

        var contentTr = `<tr>
        <td>${currentSv.ma}</td>
        <td>${currentSv.ten}</td>
        <td>${currentSv.email}</td>
        <td>${currentSv.tinhDTB()}</td>
        <td>0</td>
        <td>
        <button onclick="xoaSv(${currentSv.ma})" class="btn btn-danger">Xóa</button> 
        <button onclick="suaSv(${currentSv.ma})" class="btn btn-primary">Sửa</button> 
        </td>
        </tr>`;

        contentHTML += contentTr;
        
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function showThongTinLenForm(sv) {
    document.getElementById("txtMaSV").value = sv.ma;
    document.getElementById("txtTenSV").value = sv.ten;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.matKhau;
    document.getElementById("txtDiemLy").value = sv.diemLy;
    document.getElementById("txtDiemHoa").value = sv.diemHoa;
    document.getElementById("txtDiemToan").value = sv.diemToan;

}

function resetForm() {
    document.getElementById("formQLSV").reset();
}