var dssv = [];
const DSSV = "DSSV"

// lấy dữ liệu lên từ localStorage
var dataJson = localStorage.getItem("DSSV");
if (dataJson) {
  //truthy falsy
  var dataRaw = JSON.parse(dataJson);
  renderDssv(dssv);
}

function saveLocalStorage() {
  // lưu xuống local storage
  var dssvJson = JSON.stringify(dssv);
  localStorage.setItem("DSSV", dssvJson);
  var result = [];
  for (var index = 0; index < dataRaw.length; index++) {
    var currentData = dataRaw[index];
    var sv = new SinhVien(
      currentData.ma,
      currentData.ten,
      currentData.email,
      currentData.matKhau,
      currentData.diemLy,
      currentData.diemToan,
      currentData.diemHoa,
    );
    result.push(sv);
  }
  dssv = result;
  renderDssv(dssv);
}

function themSv() {
  var newSv = new layThongTinTuForm();
  dssv.push(newSv);
  saveLocalStorage();
  renderDssv(dssv);

  document.getElementById("formQLSV").reset();
}

// 6 giá trị falsy: null, undefined, false, "", NaN, 0

function xoaSv(idSv) {
  // indexOf: phần tử là string, number
  // findIndex: xử lý phần tử là object
  // tìm vị trí
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  
  if(index == -1) {
    return;
  }
  // xóa phần tử khỏi danh sách 
  dssv.splice(index, 1);

  // sau khi xóa thì dữ liệu thay đổi, nhưng layout ko có thay đổi vi layout dc tạo ra nhờ renderDssv
  renderDssv(dssv);
}

function suaSv(idSv) {
  var index = dssv.findIndex(function(sv) {
    return sv.ma == idSv;
  });
  
  if (index == -1) return;

  var sv = dssv[index];
  console.log('sv: ', sv);


  // show thong tin len layout
  showThongTinLenForm(sv);

  // disable input show ma sv
  document.getElementById("txtMaSV").disabled = true;
}

function capNhatSv() {
  var svEdit = layThongTinTuForm();
  
  var index = dssv.findIndex(function(sv) {
    return sv.ma == svEdit.ma;
  });

  if (index == -1) return;

  dssv[index] = svEdit;
  saveLocalStorage();
  renderDssv(dssv);
  resetForm();
  document.getElementById("txtMaSv").disabled = false;
}